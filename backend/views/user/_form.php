<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form" style="width: 300px;">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'type')->dropdownList([User::TYPE_USER => 'Пользователь', User::TYPE_ADMIN => 'Администратор']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->scenario == 'create' ? 'Добавить пользователя' : 'Обновить пользователя', ['class' => $model->scenario == 'create' ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
