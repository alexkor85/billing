<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use common\models\Transaction;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Пользователь '.$model->username;
//$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h2><?= Html::encode($this->title) ?></h2>

    <p>
        <?= Html::a('Редактировать пользователя', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Дата операции',
                'attribute' => 'time',
                'value' => function($data) { return date("Y.m.d H:i:s", $data['time']); },
            ],

            [
                'label' => 'Отправитель',
                'attribute' => 'sent_by',
                'value' => function($data) { return User::find()->where(['id' => $data['sent_by']])->one()->username; },
            ],

            [
                'label' => 'Получатель',
                'attribute' => 'received_by',
                'value' => function($data) { return User::find()->where(['id' => $data['received_by']])->one()->username; },
            ],

            [
                'label' => 'Создал операцию',
                'attribute' => 'created_by',
                'value' => function($data) { return User::find()->where(['id' => $data['created_by']])->one()->username; },
            ],

            [
                'label' => 'Сумма',
                'attribute' => 'amount',
                'value' => function($data) { return $data['amount']; },
            ],

            [
                'label' => 'Остаток',
                'attribute' => 'balance_operation',
                'value' => function($data) { return $data['balance_operation']; },
            ],

            [
                'label' => 'Тип операции',
                'attribute' => 'sender_type',
                'value' => function($data) { return Transaction::$nameOperations[$data['sender_type']]; },
            ],

        ],
    ]);  ?>

</div>
