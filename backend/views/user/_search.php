<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;
use yii\helpers\ArrayHelper;

use kartik\date\DatePicker;
?>

<div class="user-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <table class="table-filter">
        <col style="width: 20%;">
        <col style="width: 20%;">
        <col style="width: 20%;">
        <col style="width: 20%;">
        <col style="width: 20%;">
        <tr style="height: 130px;">
            <td>
                <?= $form->field($model, 'username')->dropdownList(ArrayHelper::map(User::findAll(['status' => User::STATUS_ACTIVE]), 'username', 'username'), ['prompt'=>'']) ?>
            </td>

            <td>
                <?= $form->field($model, 'balance') ?>
            </td>

            <td style="text-align: right;">
                <b style="margin-right: 10px;">Дата регистрации:</b>
            </td>

            <td>
                <b>От</b>
                <div style="margin-bottom: 15px;">
                    <?php echo DatePicker::widget([
                        'name'  => 'UserSearch[startRegisterDate]',
                        'value'  => date('d-m-Y', $model->startRegisterDate),
                        'language' => 'ru',
                        'pluginOptions' => [
                            'format' => 'dd-mm-yyyy',
                            'todayHighlight' => true
                        ]
                    ]); ?>
                </div>
            </td>

            <td>
                <b>До</b>
                <div style="margin-bottom: 15px;">
                    <?php echo DatePicker::widget([
                        'name'  => 'UserSearch[endRegisterDate]',
                        'value'  => date('d-m-Y', $model->endRegisterDate),
                        'language' => 'ru',
                        'pluginOptions' => [
                            'format' => 'dd-mm-yyyy',
                            'todayHighlight' => true
                        ]
                    ]); ?>
                </div>
            </td>
        </tr>

        <tr style="height: 130px;">
            <td>
                <?= $form->field($model, 'sum_plus') ?>
            </td>

            <td>
                <?= $form->field($model, 'sum_minus') ?>
            </td>

            <td style="text-align: right;">
                <b style="margin-right: 10px;">Обороты за период:</b>
            </td>

            <td>
                <b>От</b>
                <div style="margin-bottom: 15px;">
                    <?php echo DatePicker::widget([
                        'name'  => 'UserSearch[startPeriodDate]',
                        'value'  => date('d-m-Y', $model->startPeriodDate),
                        'language' => 'ru',
                        'pluginOptions' => [
                            'format' => 'dd-mm-yyyy',
                            'todayHighlight' => true
                        ]
                    ]); ?>
                </div>
            </td>

            <td>
                <b>До</b>
                <div style="margin-bottom: 15px;">
                    <?php echo DatePicker::widget([
                        'name'  => 'UserSearch[endPeriodDate]',
                        'value'  => date('d-m-Y', $model->endPeriodDate),
                        'language' => 'ru',
                        'pluginOptions' => [
                            'format' => 'dd-mm-yyyy',
                            'todayHighlight' => true
                        ]
                    ]); ?>
                </div>
            </td>
        </tr>


    </table>





    <div class="form-group">
        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Сбросить фильтр', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
