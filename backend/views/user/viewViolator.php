<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Нарушители';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view-violator">

    <h2><?= Html::encode($this->title) ?></h2>


    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Пользователь',
                'attribute' => 'username',
                //'value' => function($data) { return date("Y.m.d H:i:s", $data['time']); },
            ],

            [
                'label' => 'Дата',
                'attribute' => 'time',
                'value' => function($data) { return date("Y.m.d H:i:s", $data->time); },
            ],

            [
                'label' => 'Запрос',
                'attribute' => 'request',
                'value' => function($data) { return $data->request; },
            ],

        ],
    ]);  ?>

</div>
