<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::$app->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <?= $this->render('_search', ['model' => $userSearch]) ?>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $userSearch,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Дата регистрации',
                'attribute' => 'created_at',
                'value' => function($data) { return date("Y.m.d H:i:s", $data['created_at']); },
            ],

            [
                'label' => 'Email',
                'attribute' => 'username',
                'value' => function($data) { return Html::a($data['username'], Url::to(['view', 'id' => $data['id']]), []); },
                'format' => 'html',
            ],


            [
                'label' => 'Поступления',
                'attribute' => 'sum_plus',
                'value' => function($data) { return $data['sum_plus'] ? $data['sum_plus'] : '-'; },
            ],

            [
                'label' => 'Выбытия',
                'attribute' => 'sum_minus',
                'value' => function($data) { return $data['sum_minus'] ? $data['sum_minus'] : '-'; },
            ],

            [
                'label' => 'Сумма на счете',
                'attribute' => 'balance',
                'value' => function($data) { return $data['balance']; },
            ],

            [
                'label' => 'Зачислить деньги',
                'value' => function($data) { return Html::a('Зачислить деньги', Url::to(['transaction/enter-sum', 'id' => $data['id']]), ['class' => 'btn btn-success']); },
                'format' => 'html',
            ],

            [
                'label' => 'Отправить деньги',
                'value' => function($data) { return Html::a('Отправить деньги', Url::to(['transaction/create-transfer', 'id' => $data['id']]), ['class' => 'btn btn-warning']); },
                'format' => 'html',
            ],

        ],
    ]);  ?>

</div>
