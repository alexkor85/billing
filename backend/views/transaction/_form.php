<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Transaction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transaction-form" style="width: 300px;">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'received_by')->dropdownList(ArrayHelper::map(User::findAll(['status' => User::STATUS_ACTIVE]), 'id', 'username'), ['prompt'=>'']) ?>


    <div class="form-group">
        <?= Html::submitButton('Перечислить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
