<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model common\models\Transaction */

$this->title = 'Зачислить средства';
$this->params['breadcrumbs'][] = ['label' => 'Операции пользователя', 'url' => ['user/view', 'id' => $model->currentUserId]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaction-create" style="width: 300px;">

    <h2><?= Html::encode($this->title) ?></h2>

    <div class="transaction-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'amount')->textInput() ?>

        <div class="form-group">
            <?= Html::submitButton('Зачислить', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
