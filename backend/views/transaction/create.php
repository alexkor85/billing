<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Transaction */

$this->title = 'Создать перечисление';
$this->params['breadcrumbs'][] = ['label' => 'Операции пользователя', 'url' => ['user/view', 'id' => $model->currentUserId]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaction-create">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
