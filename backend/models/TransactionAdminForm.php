<?php
namespace backend\models;

use yii\base\Model;
use common\models\User;
use common\models\Transaction;
use Yii;

/**
 * Signup form
 */
class TransactionAdminForm extends Model
{
    public $amount;
    public $received_by;
    protected $_currentUserId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['amount', 'required'],
            ['amount', 'currencyFormat'],
            ['amount', 'checkBalance', 'on' => 'sendMoney'],

            ['received_by', 'required', 'on' => 'sendMoney'],
            ['received_by', 'integer'],
            ['received_by', 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => 'id'],
            ['received_by', 'compare', 'compareValue' => $this->_currentUserId, 'operator' => '!=', 'message' => 'Отправитель и получатель должны быть разными пользователями.',
                'on' => 'sendMoney'],
        ];
    }


    public function setCurrentUserId($id){
        $this->_currentUserId = $id;
    }

    public function getCurrentUserId(){
        return $this->_currentUserId;
    }

    public function currencyFormat($attribute, $params)
    {
        $this->$attribute = str_replace(',', '.', $this->$attribute);
        $this->$attribute = trim($this->$attribute);

        if(!preg_match('/^\d{1,9}\.{0,1}\d{0,2}$/', $this->$attribute)){
            $this->addError('amount', 'Неправильный формат суммы.');
            Yii::$app->getSession()->setFlash('error', 'Неправильный формат суммы.');
        }

        // отрицательная или нулевая сумма
    }

    public function checkBalance($attribute, $params)
    {
        $user = User::findOne($this->_currentUserId);

        if ($user->account->balance < $this->amount) {
            $this->addError('amount', 'Недостаточно средств на счете.');
            Yii::$app->getSession()->setFlash('error', 'Недостаточно средств на счете.');
        }
    }


    // Зачислить деньги
    public function depositMoney()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $userReceiver = User::findOne($this->_currentUserId);

            // заполняем и сохраняем операцию
            $operation = $this->_createOperation(Yii::$app->user->id, $this->_currentUserId, Yii::$app->user->id, Transaction::SENDER_TYPE_ADMIN);

            // расчитываем и обновляем баланс получателя
            $this->_updateBalanceReceiver($userReceiver, $operation);

            $transaction->commit();

        } catch(\Exception $e) {
            $transaction->rollBack();
            Yii::$app->getSession()->setFlash('error', $e->getMessage());
            return false;
        }

        return true;
    }


    // Отправить деньги
    public function sendMoney()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $userSender = User::findOne($this->_currentUserId);
            $userReceiver = User::findOne($this->received_by);

            // заполняем и сохраняем операцию
            $operation = $this->_createOperation($this->_currentUserId, $this->received_by, Yii::$app->user->id, Transaction::SENDER_TYPE_USER);

            // расчитываем и обновляем баланс отправителя
            $this->_updateBalanceSender($userSender, $operation);

            // расчитываем и обновляем баланс получателя
            $this->_updateBalanceReceiver($userReceiver, $operation);

            $transaction->commit();

        } catch(\Exception $e) {
            $transaction->rollBack();
            Yii::$app->getSession()->setFlash('error', $e->getMessage());
            return false;
        }

        return true;
    }

    protected function _createOperation($senderId, $receiverId, $creatorId, $senderType){
        $operation = new Transaction();
        $operation->time = time();
        $operation->amount = $this->amount;
        $operation->sent_by = $senderId;
        $operation->received_by = $receiverId;
        $operation->created_by = $creatorId;
        $operation->sender_type = $senderType;
        if(!$operation->save()){
            throw new \Exception('Ошибка при сохранении операции');
        }
        return $operation;
    }


    protected function _updateBalanceSender($user, $operation){
        $account = $user->account;
        $account->balance = $account->balance - $operation->amount;
        if (!$account->save()) {
            throw new \Exception('Ошибка при обновлении баланса');
        }
    }


    protected function _updateBalanceReceiver($user, $operation){
        $account = $user->account;
        $account->balance = $account->balance + $operation->amount;
        if (!$account->save()) {
            throw new \Exception('Ошибка при обновлении баланса');
        }
    }

    public function attributeLabels()
    {
        return [
            'amount' => 'Сумма',
            'received_by' => 'Получатель',
        ];
    }
}

