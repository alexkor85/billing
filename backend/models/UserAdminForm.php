<?php
namespace backend\models;

use yii\base\Model;
use common\models\User;
use common\models\Account;
use Yii;

/**
 * Signup form
 */
class UserAdminForm extends Model
{
    public $email;
    public $password;
    public $type=null;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        /*    ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],*/

            ['email', 'trim'],
            ['email', 'required', 'on' => ['create']],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique',
                'targetClass' => '\common\models\User',
                'skipOnError' => true,
                'targetAttribute' => 'username',
                'message' => 'Данный емайл уже занят.'
            ],

            ['password', 'required', 'on' => ['create']],
            ['password', 'string', 'min' => 6],

            ['type', 'required', 'on' => ['create']],
            ['type', 'in', 'range' => array_keys(User::getTypeArray())]
        ];
    }


    public function createUser()
    {
        if (!$this->validate()) {
            return null;
        }


        $transaction = Yii::$app->db->beginTransaction();
        try {
            $user = new User();
            $user->username = $this->email;
            $user->setPassword($this->password);
            $user->status = User::STATUS_ACTIVE;
            $user->type = $this->type;
            $user->generateAuthKey();
            if (!$user->save()) {
                throw new \Exception('Ошибка при сохранении пользователя');
            }

            $account = new Account;
            $account->user_id = $user->id;
            $account->balance = 0;
            if (!$account->save()) {
                throw new \Exception('Ошибка при сохранении счета');
            }

            $transaction->commit();
        } catch(\Exception $e) {
            $transaction->rollBack();
            Yii::$app->getSession()->setFlash('error', $e->getMessage());
            return null;
        }

        return $user;
    }

    public function updateUser(User $user){
        if(!empty($this->email) && $user->username != $this->email) {
            $user->username = $this->email;
        }else{
            $this->email = null;
        }

        if (!$this->validate())
            return null;

        if(!empty($this->password))
            $user->setPassword($this->password);
        if(!is_null($this->type))
            $user->type = (int) $this->type;

        return $user->save() ? $user : null;
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Email',
            'password' => 'Пароль',
        ];
    }
}
