<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use yii\data\ActiveDataProvider;
use common\components\CommonController;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use common\models\ViolatorLog;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\UserAdminForm;

use common\models\Transaction;
use common\models\UserSearch;
use common\models\Account;
use yii\data\SqlDataProvider;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends CommonController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }



    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {

    /*    $count = Yii::$app->db->createCommand('
            SELECT COUNT(*) FROM '.User::tableName().' WHERE status = :status
        ', [':status' => User::STATUS_ACTIVE])->queryScalar();

        $params = [':status' => User::STATUS_ACTIVE, ':time_begin' => 0, ':sender_type' => Transaction::SENDER_TYPE_USER];


        $sql = 'SELECT u.id, u.created_at, u.username, a.balance, t_plus.p_amount AS sum_plus, t_minus.m_amount AS sum_minus
        FROM '.User::tableName().' AS u
        LEFT JOIN '.Account::tableName().' AS a ON (a.user_id = u.id)
        LEFT JOIN     (
                SELECT t_p.received_by, SUM(t_p.amount) AS p_amount
                FROM '.Transaction::tableName().' AS t_p
                WHERE
                    t_p.time >= :time_begin
                GROUP BY t_p.received_by
            ) AS t_plus ON (t_plus.received_by = u.id)

        LEFT JOIN   (
                SELECT t_m.sent_by, SUM(t_m.amount) AS m_amount
                FROM '.Transaction::tableName().' AS t_m
                WHERE
                    t_m.sender_type = :sender_type AND
                    t_m.time >= :time_begin
                GROUP BY t_m.sent_by
            ) AS t_minus ON (t_minus.sent_by = u.id)

        WHERE
            u.status = :status
        ';

        $provider = new SqlDataProvider([
            'sql' => $sql,
            'params' => $params,
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'username' =>  [
                        'asc' => ['username' => SORT_ASC],
                        'desc' => ['username' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'e-mail',
                    ],
                    'created_at' =>  [
                        'asc' => ['created_at' => SORT_ASC],
                        'desc' => ['created_at' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'Дата регистрации',
                    ],
                    'sum_plus' =>  [
                        'asc' => ['sum_plus' => SORT_ASC],
                        'desc' => ['sum_plus' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'Поступления',
                    ],
                    'sum_minus' =>  [
                        'asc' => ['sum_minus' => SORT_ASC],
                        'desc' => ['sum_minus' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'Выбытия',
                    ],
                    'balance' =>  [
                        'asc' => ['balance' => SORT_ASC],
                        'desc' => ['balance' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'Сумма на счете',
                    ],
                ],
            ],
        ]);*/


        $userSearch = new UserSearch();
        $dataProvider = $userSearch->search(Yii::$app->request->get());

        return $this->render('index', [
            //'model' => $this->findModel($id),
            'dataProvider' => $dataProvider,
            'userSearch' => $userSearch,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $params = [':user_id' => $id, ':sender_type' => Transaction::SENDER_TYPE_USER];

        $count = Yii::$app->db->createCommand('
            SELECT COUNT(*) FROM '.Transaction::tableName().' WHERE (sent_by=:user_id AND sender_type=:sender_type) OR received_by=:user_id
        ', $params)->queryScalar();



        $sql = 'SELECT t.id, t.time, t.amount, t.sent_by, t.received_by, t.created_by, t.sender_type, SUM(CASE WHEN t2.received_by = :user_id THEN  t2.amount ELSE 0 END) - SUM(CASE WHEN t2.sent_by = :user_id AND t2.sender_type=:sender_type THEN  t2.amount ELSE 0 END) AS balance_operation
        FROM '.Transaction::tableName().' AS t, '.Transaction::tableName().' AS t2
        WHERE
            ((t.sent_by=:user_id AND t.sender_type=:sender_type) OR t.received_by=:user_id) AND
            ((t2.sent_by=:user_id AND t2.sender_type=:sender_type) OR t2.received_by=:user_id) AND
            t.id >= t2.id
        GROUP BY t.id
        ORDER BY t.id ASC
        ';

        $provider = new SqlDataProvider([
            'sql' => $sql,
            'params' => $params,
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'time' => SORT_ASC,
                ],
            ],
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $provider,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserAdminForm(['scenario' => 'create']);
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->createUser()) {
                Yii::$app->getSession()->setFlash('success', 'Пользователь успешно добавлен.');
                return $this->redirect(['view', 'id' => $user->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $user = $this->findModel($id);

        $model = new UserAdminForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->updateUser($user)) {
                Yii::$app->getSession()->setFlash('success', 'Данные пользователя успешно обновлены.');
                return $this->redirect(['view', 'id' => $user->id]);
            }
        }

        $model->email = $user->username;
        $model->type = $user->type;

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionViewViolator()
    {
        $query = ViolatorLog::find()->joinWith('user')->where([]);//->andFilterWhere(['like', 'user.username', $this->city]);

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => [
                    'time' => SORT_DESC,
                ],
                'attributes' => [
                    'username' =>  [
                        'asc' => ['user.username' => SORT_ASC],
                        'desc' => ['user.username' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'e-mail',
                    ],
                    'time' => [
                        'asc' => ['time' => SORT_ASC],
                        'desc' => ['time' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'Дата',
                    ],
                    'request' => [
                        'asc' => ['request' => SORT_ASC],
                        'desc' => ['request' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'Запрос',
                    ],
                ],
            ],
        ]);

        return $this->render('viewViolator', [
            'dataProvider' => $provider,
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
