<?php

namespace backend\controllers;

use Yii;
use common\models\Transaction;
use common\models\TransactionSearch;
use common\components\CommonController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\TransactionAdminForm;
use yii\web\ForbiddenHttpException;
use common\models\ViolatorLog;
use common\models\User;

/**
 * TransactionController implements the CRUD actions for Transaction model.
 */
class TransactionController extends CommonController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }



    /**
     * Creates a new Transaction model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateTransfer($id)
    {
        $model = new TransactionAdminForm(['scenario' => 'sendMoney']);
        $model->currentUserId = $id;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->sendMoney();
            Yii::$app->session->setFlash('success', 'Операция успешно проведена.');
            return $this->redirect(['user/view', 'id' => $id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionEnterSum($id)
    {
        $model = new TransactionAdminForm();
        $model->currentUserId = $id;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->depositMoney();
            Yii::$app->session->setFlash('success', 'Операция успешно проведена.');
            return $this->redirect(['user/view', 'id' => $id]);
        } else {
            return $this->render('enterSum', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the Transaction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Transaction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Transaction::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
