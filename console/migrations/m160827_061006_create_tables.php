<?php

use yii\db\Migration;

class m160827_061006_create_tables extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'type' => $this->smallInteger()->notNull()->defaultValue(0),
            'email_confirm_token' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('key-username', '{{%user}}', 'username');


        $this->createTable('{{%account}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull()->unique(),
            'balance' => $this->decimal(18,2)->notNull()->unsigned(),
        ], $tableOptions);

        $this->createIndex('key_user_id','{{%account}}','user_id');
        $this->addForeignKey("user_id_fk", "{{%account}}", "user_id", "{{%user}}", "id", 'RESTRICT', 'RESTRICT');


        $this->createTable('{{%transaction}}', [
            'id' => $this->primaryKey(),
            //'user_id' => $this->integer()->notNull(),
            //'account_id' => $this->integer()->notNull(),
            'time' => $this->integer()->notNull(),
            'amount' => $this->decimal(18,2)->notNull()->unsigned(),
            'sent_by' => $this->integer()->notNull(),
            'received_by' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'sender_type' => $this->smallInteger()->notNull()->defaultValue(0),
        ], $tableOptions);

        //$this->createIndex('key_account_id','{{%transaction}}','account_id');
        $this->createIndex('key_sent_by','{{%transaction}}','sent_by');
        $this->createIndex('key_received_by','{{%transaction}}','received_by');
        //$this->addForeignKey("account_fk", "{{%transaction}}", "account_id", "{{%account}}", "id", 'RESTRICT', 'RESTRICT');
        $this->addForeignKey("sent_by_fk", "{{%transaction}}", "sent_by", "{{%user}}", "id", 'RESTRICT', 'RESTRICT');
        $this->addForeignKey("received_by_fk", "{{%transaction}}", "received_by", "{{%user}}", "id", 'RESTRICT', 'RESTRICT');


        $this->createTable('{{%violator_log}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'time' => $this->integer()->notNull(),
            'request' => $this->string(),
        ], $tableOptions);


        $this->insert('{{%user}}', [
            'username' => 'admin@mail.ru',
            'auth_key' => Yii::$app->security->generateRandomString(),
            'password_hash' => Yii::$app->security->generatePasswordHash('123456'),
            //'password_reset_token' => '',
            'status' => 1,
            'type' => 1,
            //'email_confirm_token' => '',
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        $this->insert('{{%account}}', [
            'user_id' => 1,
            'balance' => 0.00,
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%transaction}}');
        $this->dropTable('{{%account}}');
        $this->dropTable('{{%user}}');
        $this->dropTable('{{%violator_log}}');
    }

}
