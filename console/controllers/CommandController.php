<?php
namespace console\controllers;
use common\models\Transaction;
use Yii;
use yii\console\Controller;
use common\components\rbac\UserRoleRule;
use common\models\User;
use common\models\Account;

class CommandController extends Controller
{
    public function actionCreateRbac()
    {
        $authManager = Yii::$app->authManager;

        //удаляем старые данные
        $authManager->removeAll();

        // Create roles
        $guest  = $authManager->createRole('guest');
        $user = $authManager->createRole('user');
        $admin  = $authManager->createRole('admin');

        $permissions = [];

        // разрешениия для guest
        $permissions['guest'][] = $authManager->createPermission('site/login');
        $permissions['guest'][] = $authManager->createPermission('site/logout');
        $permissions['guest'][] = $authManager->createPermission('site/signup');
        $permissions['guest'][] = $authManager->createPermission('site/email-confirm');
        $permissions['guest'][] = $authManager->createPermission('site/request-password-reset');
        $permissions['guest'][] = $authManager->createPermission('site/reset-password');
        $permissions['guest'][] = $authManager->createPermission('site/error');

        // разрешениия для user
        $permissions['user'][] = $authManager->createPermission('site/index');

        $permissions['user'][] = $authManager->createPermission('transaction/index');
        $permissions['user'][] = $authManager->createPermission('transaction/create');

        // разрешениия для admin
        $permissions['admin'][] = $authManager->createPermission('user/index');
        $permissions['admin'][] = $authManager->createPermission('user/view');
        $permissions['admin'][] = $authManager->createPermission('user/create');
        $permissions['admin'][] = $authManager->createPermission('user/update');
        $permissions['admin'][] = $authManager->createPermission('user/view-violator');

        $permissions['admin'][] = $authManager->createPermission('transaction/create-transfer');
        $permissions['admin'][] = $authManager->createPermission('transaction/enter-sum');

        //$error  = $authManager->createPermission('error');

        // Добавить все разрешения
        foreach($permissions as $userTypePermission){
            foreach($userTypePermission as $permission){
                $authManager->add($permission);
            }
        }
        //$authManager->add($error);

        // добавить правило
        $userGroupRule = new UserRoleRule();
        $authManager->add($userGroupRule);

        //$guest->ruleName  = $userGroupRule->name;
        $user->ruleName = $userGroupRule->name;
        $admin->ruleName  = $userGroupRule->name;

        // Add roles in Yii::$app->authManager
        $authManager->add($guest);
        $authManager->add($user);
        $authManager->add($admin);

        // добавить разрешениия для guest
        //$authManager->addChild($guest, $error);
        foreach($permissions['guest'] as $permission){
            $authManager->addChild($guest, $permission);
        }

        // добавить разрешениия для user
        foreach($permissions['user'] as $permission){
            $authManager->addChild($user, $permission);
        }
        $authManager->addChild($user, $guest);

        // добавить разрешениия для admin
        foreach($permissions['admin'] as $permission){
            $authManager->addChild($admin, $permission);
        }
        $authManager->addChild($admin, $user);

    }

    public function actionFillDb()
    {
        ini_set('memory_limit', '-1');
        set_time_limit(1200);
        bcscale(2);

        $users = [];
        $usersTemp = [];
        $accounts = [];
        $passwordHash = Yii::$app->security->generatePasswordHash('123456');
        $currentTime = time() - 1000000;

        $transaction = Yii::$app->db->beginTransaction();
        try {

            // последний id пользователя
            $userId = Yii::$app->db->createCommand('SELECT max([[id]]) AS id FROM {{user}}')->queryOne()['id'];
            //echo '$userId='.$userId;

            for($i=0; $i < 1000; $i++){

                $userId++;
                $user = [];
                $account = [];

                $user['username'] = 'user_'.$userId.'@m.ru';
                $user['password_hash'] = $passwordHash;
                $user['auth_key'] = Yii::$app->security->generateRandomString();
                $user['status'] = User::STATUS_ACTIVE;
                $user['type'] = User::TYPE_USER;
                $user['created_at'] = $currentTime;
                $user['updated_at'] = $currentTime;

                $account['user_id'] = $userId;
                $account['balance'] = 1000000.00;

                $usersTemp[$i] = $user;
                $users[$i] = ['id' => $userId, 'balance' => $account['balance']];
                $accounts[$i] = $account;
            }


            $innerTransaction = Yii::$app->db->beginTransaction();
            try {
                Yii::$app->db->createCommand()->batchInsert('user', ['username', 'password_hash', 'auth_key', 'status', 'type', 'created_at', 'updated_at'], $usersTemp)->execute();
                $innerTransaction->commit();
            } catch (\Exception $e) {
                $innerTransaction->rollBack();
                throw new \Exception('Ошибка при сохранении пользователей:'.$e->getMessage());
            }

            $innerTransaction = Yii::$app->db->beginTransaction();
            try {
                Yii::$app->db->createCommand()->batchInsert('account', ['user_id', 'balance'], $accounts)->execute();
                $innerTransaction->commit();
            } catch (\Exception $e) {
                $innerTransaction->rollBack();
                throw new \Exception('Ошибка при сохранении счетов:'.$e->getMessage());
            }

            unset($usersTemp);

            // создать операции для первоначального баланса
            $operations = [];
            for($i=0; $i < 1000; $i++) {
                $operations[] = [
                    'time' => $currentTime,
                    'amount' => 1000000.00,
                    'sent_by' => 1,
                    'received_by' => $users[$i]['id'],
                    'created_by' => 1,
                    'sender_type' => Transaction::SENDER_TYPE_ADMIN,
                ];
            }

            $innerTransaction = Yii::$app->db->beginTransaction();
            try {
                Yii::$app->db->createCommand()->batchInsert('transaction', ['time', 'amount', 'sent_by', 'received_by', 'created_by', 'sender_type'], $operations)->execute();
                $innerTransaction->commit();
            } catch (\Exception $e) {
                $innerTransaction->rollBack();
                throw new \Exception('Ошибка при сохранении операций:'.$e->getMessage());
            }


            for($m = 0; $m < 100; $m++){

                $operations = [];

                for($n = 0; $n < 10000; $n++){

                    // случайным образом выбрать отправителя
                    $userSenderNumber = mt_rand(0, 999);
                    // если у отправителя баланс меньше, выбрать другого
                    if($users[$userSenderNumber]['balance'] < 100.00){
                        $n--;
                        continue;
                    }
                    // случайным образом выбрать получателя
                    $userReceiverNumber = mt_rand(0, 999);
                    if($userReceiverNumber == $userSenderNumber){
                        $n--;
                        continue;
                    }
                    // создать сумму операции, 1-20% от баланса
                    $amount = bcdiv($users[$userSenderNumber]['balance']*mt_rand(1, 20), 100);
                    $users[$userReceiverNumber]['balance'] = bcadd($users[$userReceiverNumber]['balance'], $amount);
                    $users[$userSenderNumber]['balance'] = bcsub($users[$userSenderNumber]['balance'], $amount);

                    $operations[] = [
                        'time' => $currentTime++,
                        'amount' => $amount,
                        'sent_by' => $users[$userSenderNumber]['id'],
                        'received_by' => $users[$userReceiverNumber]['id'],
                        'created_by' => $users[$userSenderNumber]['id'],
                        'sender_type' => Transaction::SENDER_TYPE_USER,
                    ];
                }

                $innerTransaction = Yii::$app->db->beginTransaction();
                try {
                    Yii::$app->db->createCommand()->batchInsert('transaction', ['time', 'amount', 'sent_by', 'received_by', 'created_by', 'sender_type'], $operations)->execute();
                    $innerTransaction->commit();
                } catch (\Exception $e) {
                    $innerTransaction->rollBack();
                    throw new \Exception('Ошибка при сохранении операций:'.$e->getMessage());
                }
                echo $m.' Save 10 000 operations'."\n";
            }


            foreach($users as $user){
                Yii::$app->db->createCommand()->update('account', ['balance' => $user['balance']], 'id=:id', [':id' => $user['id']])->execute();
            }

            $transaction->commit();

        } catch(\Exception $e) {
            $transaction->rollBack();
            Yii::error($e->getMessage());
        }
    }
}