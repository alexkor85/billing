<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use common\models\User;

/**
 * TransactionSearch represents the model behind the search form about `app\models\Transaction`.
 */
class UserSearch extends User
{
    //public $created_at;
    public $balance;
    public $sum_plus;
    public $sum_minus;
    public $startRegisterDate;
    public $endRegisterDate;
    public $startPeriodDate;
    public $endPeriodDate;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'], 'email'],
            [['startRegisterDate', 'endRegisterDate', 'startPeriodDate', 'endPeriodDate'], 'safe'],
            [['sum_plus', 'sum_minus', 'balance'], 'number'],
            [['sum_plus', 'sum_minus', 'balance'], 'currencyFormat'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function currencyFormat($attribute, $params)
    {
        $this->$attribute = str_replace(',', '.', $this->$attribute);
        $this->$attribute = trim($this->$attribute);

        if(!preg_match('/^\d{1,9}\.{0,1}\d{0,2}$/', $this->$attribute)){
            $this->addError('amount', 'Неправильный формат суммы.');
            Yii::$app->getSession()->setFlash('error', 'Неправильный формат суммы.');
        }
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $selectCount = 'SELECT COUNT(*)';
        $select = 'SELECT u.id, u.created_at, u.username, a.balance, t_plus.sum_plus, t_minus.sum_minus';

        $sql = '
        FROM '.User::tableName().' AS u
        LEFT JOIN '.Account::tableName().' AS a ON (a.user_id = u.id)
        LEFT JOIN     (
                SELECT t_p.received_by, SUM(t_p.amount) AS sum_plus
                FROM '.Transaction::tableName().' AS t_p
                WHERE
                    t_p.time >= :time_begin AND
                    t_p.time <= :time_end
                GROUP BY t_p.received_by
            ) AS t_plus ON (t_plus.received_by = u.id)

        LEFT JOIN   (
                SELECT t_m.sent_by, SUM(t_m.amount) AS sum_minus
                FROM '.Transaction::tableName().' AS t_m
                WHERE
                    t_m.sender_type = :sender_type AND
                    t_m.time >= :time_begin AND
                    t_m.time <= :time_end
                GROUP BY t_m.sent_by
            ) AS t_minus ON (t_minus.sent_by = u.id)

        WHERE
            u.status = :status
        ';

        $params = [':status' => User::STATUS_ACTIVE, ':time_begin' => 0, ':time_end' =>mktime(0, 0, 0, date("n"), date("j")+1, date("Y")), ':sender_type' => Transaction::SENDER_TYPE_USER];

        if ($this->validate()) {
            if(!empty($this->username)) {
                $sql .= ' AND u.username=:username';
                $params = array_merge($params, [':username' => $this->username]);
            }

            if(!empty($this->balance)) {
                $sql .= ' AND a.balance=:balance';
                $params = array_merge($params, [':balance' => $this->balance]);
            }

            if(!empty($this->sum_plus)) {
                $sql .= ' AND sum_plus=:sum_plus';
                $params = array_merge($params, [':sum_plus' => $this->sum_plus]);
            }

            if(!empty($this->sum_minus)) {
                $sql .= ' AND sum_minus=:sum_minus';
                $params = array_merge($params, [':sum_minus' => $this->sum_minus]);
            }

            if(!empty($this->startRegisterDate)) {
                $sql .= ' AND created_at>=:startRegisterDate';
                $this->startRegisterDate = strtotime(date('Y-m-d  00:00:00', strtotime($this->startRegisterDate)));
                $params = array_merge($params, [':startRegisterDate' => $this->startRegisterDate]);
            }else{
                $this->startRegisterDate = strtotime('2016-01-01 00:00:00');
            }

            if(!empty($this->endRegisterDate)) {
                $sql .= ' AND created_at<=:endRegisterDate';
                $this->endRegisterDate = strtotime(date('Y-m-d  23:59:59', strtotime($this->endRegisterDate)));
                $params = array_merge($params, [':endRegisterDate' => $this->endRegisterDate]);
            }else{
                //$this->startRegisterDate = strtotime(date('+1 day'));
                $this->endRegisterDate = mktime(0, 0, 0, date("n"), date("j")+1, date("Y"));
            }

            if(!empty($this->startPeriodDate)) {
                $this->startPeriodDate = strtotime(date('Y-m-d  00:00:00', strtotime($this->startPeriodDate)));
                $params = array_merge($params, [':time_begin' => $this->startPeriodDate]);
            }else{
                $this->startPeriodDate = strtotime('2016-01-01 00:00:00');
                $params = array_merge($params, [':time_begin' => $this->startPeriodDate]);
            }

            if(!empty($this->endPeriodDate)) {
                $this->endPeriodDate = strtotime(date('Y-m-d  23:59:59', strtotime($this->endPeriodDate)));
                $params = array_merge($params, [':time_end' => $this->endPeriodDate]);
            }else{
                //$this->endPeriodDate = strtotime(date('+1 day'));
                $this->endPeriodDate = mktime(0, 0, 0, date("n"), date("j")+1, date("Y"));
                $params = array_merge($params, [':time_end' => $this->endPeriodDate]);
            }
        }else{
            $this->startRegisterDate = strtotime('2016-01-01 00:00:00');
            $this->endRegisterDate = mktime(0, 0, 0, date("n"), date("j")+1, date("Y"));
            $this->startPeriodDate = strtotime('2016-01-01 00:00:00');
            $this->endPeriodDate = mktime(0, 0, 0, date("n"), date("j")+1, date("Y"));
        }

        $count = Yii::$app->db->createCommand($selectCount.$sql, $params)->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql' => $select.$sql,
            'params' => $params,
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'username' =>  [
                        'asc' => ['username' => SORT_ASC],
                        'desc' => ['username' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'e-mail',
                    ],
                    'created_at' =>  [
                        'asc' => ['created_at' => SORT_ASC],
                        'desc' => ['created_at' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'Дата регистрации',
                    ],
                    'sum_plus' =>  [
                        'asc' => ['sum_plus' => SORT_ASC],
                        'desc' => ['sum_plus' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'Поступления',
                    ],
                    'sum_minus' =>  [
                        'asc' => ['sum_minus' => SORT_ASC],
                        'desc' => ['sum_minus' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'Выбытия',
                    ],
                    'balance' =>  [
                        'asc' => ['balance' => SORT_ASC],
                        'desc' => ['balance' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'Сумма на счете',
                    ],
                ],
            ],
        ]);

        return $dataProvider;
    }
}
