<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "violator_log".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $time
 * @property string $request
 *
 * @property User $user
 * @property Transaction $transaction
 */
class ViolatorLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'violator_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'time' => 'Дата',
            'request' => 'Запрос',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUserName()
    {
        return $this->user->username;
    }
}
