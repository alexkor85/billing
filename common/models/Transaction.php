<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "transaction".
 *
 * @property integer $id
 * @property integer $account_id
 * @property integer $time
 * @property string $amount
 * @property integer $sent_by
 * @property integer $received_by
 * @property integer $created_by
 * @property integer $sender_type
 *
 * @property Account $account
 * @property User $sentBy
 * @property User $receivedBy
 */
class Transaction extends \yii\db\ActiveRecord
{
    const SENDER_TYPE_USER = 0;
    const SENDER_TYPE_ADMIN = 1;

    public static $nameOperations = [
        self::SENDER_TYPE_USER => 'Перечисление',
        self::SENDER_TYPE_ADMIN => 'Зачислено администратором',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount', 'received_by'], 'required'],
            [['received_by',], 'integer'],
            [['amount'], 'number'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'time' => 'Дата и время',
            'amount' => 'Сумма',
            'sent_by' => 'Отправитель',
            'received_by' => 'Получатель',
            'created_by' => 'Создатель',
            'sender_type' => 'Тип операции',
        ];
    }


    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['id' => 'account_id']);
    }


    public function getUser()
    {
        /*return $this->hasOne(User::className(), ['id' => 'user_id'])
            ->viaTable('account', ['user_id' => 'account_id']);*/
        return $this->account->user;
    }


    public function getSentBy()
    {
        return $this->hasOne(User::className(), ['id' => 'sent_by']);
    }


    public function getReceivedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'received_by']);
    }

}
