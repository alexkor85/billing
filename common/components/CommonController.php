<?php

namespace common\components;

use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use common\models\ViolatorLog;
use common\models\User;


class CommonController extends Controller
{
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (!Yii::$app->user->can($this->id . '/' . $action->id)) {
                if (Yii::$app->user->isGuest && $action != 'login' && $action != 'error') {
                    Yii::$app->getUser()->setReturnUrl(Yii::$app->getRequest()->getUrl());
                    Yii::$app->user->loginRequired();
                } // если user, то сохранить в журнал
                elseif (Yii::$app->user->identity->type == User::TYPE_USER) {
                    $violator = new ViolatorLog;
                    $violator->user_id = Yii::$app->user->id;
                    $violator->time = time();
                    $violator->request = Yii::$app->request->absoluteUrl;
                    $violator->save();
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
            return true;
        } else {
            return false;
        }
    }
}