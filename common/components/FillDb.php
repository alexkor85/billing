<?php
namespace common\components;
use Yii;
use yii\console\Controller;
use common\components\rbac\UserRoleRule;
use common\models\User;
use common\models\Account;

class FillDb
{

    public function run()
    {
        ini_set('memory_limit', '-1');
        set_time_limit(600);
        bcscale(2);

        $users = [];
        $usersTemp = [];
        $accounts = [];
        $passwordHash = Yii::$app->security->generatePasswordHash('123456');
        $currentTime = time();

        $transaction = Yii::$app->db->beginTransaction();
        try {

            // последний id пользователя
            $userId = Yii::$app->db->createCommand('SELECT max([[id]]) AS id FROM {{user}}')->queryOne()['id'];
            echo '$userId='.$userId;

            for($i=1; $i <= 1000; $i++){

                $userId++;
                $user = [];
                $account = [];

                $user['username'] = 'user_'.$i.'@m.ru';
                $user['password_hash'] = $passwordHash;
                $user['auth_key'] = Yii::$app->security->generateRandomString();
                $user['status'] = User::STATUS_ACTIVE;
                $user['type'] = User::TYPE_USER;
                $user['created_at'] = $currentTime;
                $user['updated_at'] = $currentTime;

                $account['user_id'] = $userId;
                $account['balance'] = 1000000.00;

                $usersTemp[$i] = $user;
                $users[$i] = ['id' => $userId, 'balance' => $account['balance']];
                $accounts[$i] = $account;


                /*    $user = new User();
                    $user->username = 'user_'.$i.'@m.ru';
                    $user->setPassword('123456');
                    $user->status = User::STATUS_ACTIVE;
                    $user->generateAuthKey();
                    if (!$user->save()) {
                        throw new \Exception('Ошибка при сохранении пользователя');
                    }

                    $account = new Account;
                    $account->user_id = $user->id;
                    $account->balance = 0;
                    if (!$account->save()) {
                        throw new \Exception('Ошибка при сохранении счета');
                    }*/
            }

            // сохранение ОПЕРАЦИЙ на 1000000 !!!!!!!!!!!!!!!!!!!!!!!!!!
            Yii::$app->db->createCommand()->batchInsert('user', ['username', 'password_hash', 'auth_key', 'status', 'type', 'created_at', 'updated_at'], $usersTemp)->execute();
            $transaction->commit();
            Yii::$app->db->createCommand()->batchInsert('account', ['user_id', 'balance'], $accounts)->execute();

            /*        unset($usersTemp);

                    for($m = 1; $m <= 100; $m++){

                        $operations = [];

                        for($n = 1; $n <= 10000; $n++){
                            $operation = [];
                            // случайным образом выбрать отправителя
                            // если у отправителя баланс меньше
                            // случайным образом выбрать получателя
                            $operations[] = $operation;
                        }

                        Yii::$app->db->createCommand()->batchInsert('transaction', ['time', 'amount', 'sent_by', 'received_by', 'created_by', 'sender_type'], $operations)->execute();
                    }


                    foreach($users as $user){
                        Yii::$app->db->createCommand()->update('account', ['balance' => $user['balance']], 'id=:id', [':user_id' => $user['id']])->execute();
                    }*/

            $transaction->commit();

        } catch(\Exception $e) {
            $transaction->rollBack();
            Yii::error($e->getMessage());
        }
    }
}