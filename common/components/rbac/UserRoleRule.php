<?php
namespace common\components\rbac;
use Yii;
use yii\rbac\Rule;
use yii\helpers\ArrayHelper;
use common\models\User;

class UserRoleRule extends Rule
{
    public $name = 'userRole';
    public function execute($user, $item, $params)
    {
        if(isset(Yii::$app->user->identity->type)){
            $userRole = Yii::$app->user->identity->type;
        }else{
            $userRole = 666;
        }

        if($item->name === 'admin') {
            return $userRole == User::TYPE_ADMIN;
        } elseif ($item->name === 'user') {
            return $userRole == User::TYPE_ADMIN || $userRole == User::TYPE_USER;
        }

        return true;
    }
}