<?php
return [
    'site/login' => [
        'type' => 2,
    ],
    'site/logout' => [
        'type' => 2,
    ],
    'site/signup' => [
        'type' => 2,
    ],
    'site/email-confirm' => [
        'type' => 2,
    ],
    'site/request-password-reset' => [
        'type' => 2,
    ],
    'site/reset-password' => [
        'type' => 2,
    ],
    'site/error' => [
        'type' => 2,
    ],
    'site/index' => [
        'type' => 2,
    ],
    'transaction/index' => [
        'type' => 2,
    ],
    'transaction/create' => [
        'type' => 2,
    ],
    'user/index' => [
        'type' => 2,
    ],
    'user/view' => [
        'type' => 2,
    ],
    'user/create' => [
        'type' => 2,
    ],
    'user/update' => [
        'type' => 2,
    ],
    'user/view-violator' => [
        'type' => 2,
    ],
    'transaction/create-transfer' => [
        'type' => 2,
    ],
    'transaction/enter-sum' => [
        'type' => 2,
    ],
    'guest' => [
        'type' => 1,
        'children' => [
            'site/login',
            'site/logout',
            'site/signup',
            'site/email-confirm',
            'site/request-password-reset',
            'site/reset-password',
            'site/error',
        ],
    ],
    'user' => [
        'type' => 1,
        'ruleName' => 'userRole',
        'children' => [
            'site/index',
            'transaction/index',
            'transaction/create',
            'guest',
        ],
    ],
    'admin' => [
        'type' => 1,
        'ruleName' => 'userRole',
        'children' => [
            'user/index',
            'user/view',
            'user/create',
            'user/update',
            'user/view-violator',
            'transaction/create-transfer',
            'transaction/enter-sum',
            'user',
        ],
    ],
];
