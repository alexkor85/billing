<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;
use common\models\Transaction;
use Yii;

/**
 * Signup form
 */
class TransactionUserForm extends Model
{
    public $amount;
    public $received_by;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount', 'received_by'], 'required'],
            ['received_by', 'integer'],
            /*['amount', 'number'],*/
            ['amount', 'currencyFormat'],
            /*['amount', 'compare', 'skipOnError' => true, 'compareValue' => 0, 'operator' => '>', 'message' => 'Нельзя перечислить отрицательную или нулевую сумму.'],*/
            ['amount', 'checkBalance'],
            [['received_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => 'id'],
            //    'filter' => 'id<>'.Yii::$app->user->id] // ['received_by<>:currentUser', [':currentUser' => Yii::$app->user->id]]],
            ['received_by', 'compare', 'compareValue' => Yii::$app->user->id, 'operator' => '!=', 'message' => 'Нельзя перечислять средства самому себе.']
        ];
    }

    public function currencyFormat($attribute, $params)
    {
        $this->$attribute = str_replace(',', '.', $this->$attribute);
        $this->$attribute = trim($this->$attribute);

        if(!preg_match('/^\d{1,9}\.{0,1}\d{0,2}$/', $this->$attribute)){
            $this->addError('amount', 'Неправильный формат суммы.');
            Yii::$app->getSession()->setFlash('error', 'Неправильный формат суммы.');
        }
    }

    public function checkBalance($attribute, $params)
    {
        $user = User::findOne(Yii::$app->user->id);

        if ($user->account->balance < $this->amount) {
            $this->addError('amount', 'Недостаточно средств на счете.');
            Yii::$app->getSession()->setFlash('error', 'Недостаточно средств на счете.');
        }
    }

    public function createTransaction()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $userSender = User::findOne(Yii::$app->user->id);
            $userReceiver = User::findOne($this->received_by);

            // заполняем и сохраняем операцию
            $operation = $this->_createOperation($userSender->id, $userReceiver->id, $userSender->id, Transaction::SENDER_TYPE_USER);

            // расчитываем и обновляем баланс отправителя
            $this->_updateBalanceSender($userSender/*, $operation*/);

            // расчитываем и обновляем баланс получателя
            $this->_updateBalanceReceiver($userReceiver/*, $operation*/);

            $transaction->commit();

        } catch(\Exception $e) {
            $transaction->rollBack();
            Yii::$app->getSession()->setFlash('error', $e->getMessage());
            Yii::error($e->getMessage());
            return false;
        }

        return true;
    }

    protected function _createOperation($senderId, $receiverId, $creatorId, $senderType){
        $operation = new Transaction();
        $operation->time = time();
        $operation->amount = $this->amount;
        $operation->sent_by = $senderId;
        $operation->received_by = $receiverId;
        $operation->created_by = $creatorId;
        $operation->sender_type = $senderType;
        if(!$operation->save()){
            throw new \Exception('Ошибка при сохранении операции');
        }
        return $operation;
    }

    protected function _updateBalanceSender($user/*, $operation*/){
        $account = $user->account;
        $account->balance = $account->balance - $this->amount;
        if (!$account->save()) {
            throw new \Exception('Ошибка при обновлении баланса');
        }
    }

    protected function _updateBalanceReceiver($user/*, $operation*/){
        $account = $user->account;
        $account->balance = $account->balance + $this->amount;
        if (!$account->save()) {
            throw new \Exception('Ошибка при обновлении баланса');
        }
    }

    public function attributeLabels()
    {
        return [
            'amount' => 'Сумма',
            'received_by' => 'Получатель',
        ];
    }
}
