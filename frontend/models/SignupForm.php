<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;
use common\models\Account;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    //public $username;
    public $email;
    public $password;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        /*    ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],*/

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique',
                'targetClass' => '\common\models\User',
                'targetAttribute' => 'username',
                'message' => 'Данный емайл уже занят.'
            ],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
/*    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }*/

    public function signup()
    {
        if ($this->validate()) {

            $transaction = Yii::$app->db->beginTransaction();
            try {
                $user = new User();
                $user->username = $this->email;
                $user->setPassword($this->password);
                $user->status = User::STATUS_WAIT;
                $user->generateAuthKey();
                $user->generateEmailConfirmToken();
                if (!$user->save()) {
                    throw new \Exception('Ошибка при сохранении пользователя');
                }

                $account = new Account;
                $account->user_id = $user->id;
                $account->balance = 0;
                if (!$account->save()) {
                    throw new \Exception('Ошибка при сохранении счета');
                }

                $result = true;
                $transaction->commit();
            } catch(\Exception $e) {
                $transaction->rollBack();
                $result = false;
            }

            if ($result) {
                Yii::$app->mailer->compose(
                        ['html' => 'emailConfirm-html', 'text' => 'emailConfirm-text'],
                        ['user' => $user]
                    )
                    ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                    ->setTo($this->email)
                    ->setReplyTo([$this->email => $this->email])
                    ->setSubject('Email confirmation for ' . Yii::$app->name)
                    ->send();
                return $user;
            }
        }

        return null;
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Email',
            'password' => 'Пароль',
        ];
    }
}
