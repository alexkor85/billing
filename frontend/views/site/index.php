<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::$app->name;
?>
<div class="site-index">

    <div class="jumbotron">
        <h3><?php echo Yii::$app->name; ?></h3>

        <p class="lead">
            Для работы с приложением выполните
            <?php echo Html::a('Вход', Url::toRoute(['site/login'])); ?>
            <br />
            Если вы не зарегистрированны, выберите
            <?php echo Html::a('Регистрация', Url::toRoute(['site/signup'])); ?>
        </p>
    </div>

</div>
