<?php

namespace frontend\controllers;

use Yii;
use common\models\Transaction;
use common\models\TransactionSearch;
use yii\web\ForbiddenHttpException;
use common\models\User;
use common\components\CommonController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\TransactionUserForm;
use yii\data\SqlDataProvider;
use yii\data\Sort;

/**
 * TransactionController implements the CRUD actions for Transaction model.
 */
class TransactionController extends CommonController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Lists all Transaction models.
     * @return mixed
     */
    public function actionIndex()
    {

        $user = User::findOne(Yii::$app->user->id);

        $params = [':user_id' => Yii::$app->user->id, ':sender_type' => Transaction::SENDER_TYPE_USER];

        $count = Yii::$app->db->createCommand('
            SELECT COUNT(*) FROM '.Transaction::tableName().' WHERE (sent_by=:user_id AND sender_type=:sender_type) OR received_by=:user_id
        ', $params)->queryScalar();



        $sql = 'SELECT t.id, t.time, t.amount, t.sent_by, t.received_by, t.created_by, t.sender_type, SUM(CASE WHEN t2.received_by = :user_id THEN  t2.amount ELSE 0 END) - SUM(CASE WHEN t2.sent_by = :user_id AND t2.sender_type=:sender_type THEN  t2.amount ELSE 0 END) AS balance_operation
        FROM '.Transaction::tableName().' AS t, '.Transaction::tableName().' AS t2
        WHERE
            ((t.sent_by=:user_id AND t.sender_type=:sender_type) OR t.received_by=:user_id) AND
            ((t2.sent_by=:user_id AND t2.sender_type=:sender_type) OR t2.received_by=:user_id) AND
            t.id >= t2.id
        GROUP BY t.id
        ORDER BY t.id ASC
        ';


        $provider = new SqlDataProvider([
            'sql' => $sql,
            'params' => $params,
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'time' => SORT_ASC,
                ],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $provider,
            'user' => $user,
        ]);
    }

    /**
     * Displays a single Transaction model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Transaction model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TransactionUserForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->createTransaction()){
                return $this->redirect(['index']);
            }else{
                return $this->redirect(['create', [
                    'model' => $model,
                ]]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Finds the Transaction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Transaction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Transaction::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
